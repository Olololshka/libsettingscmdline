Библиотека обработчик аргументов командной строки и хранитель настрек.
Является комбинацией обьектов QSettings и SimpleOpt (http://code.google.com/p/simpleopt/)
Внутренная структура унаследована от QVariantMap.

Требуется:
    * >=Qt-4.8.0
    * >=cmake-4.8
    * >=doxygen-1.8 (Документация)
    * >=git-1.7 (Получение исходных кодов)
	
Сборка:
    * Скачать исхохный код при помощи git
    $ git clone git clone https://bitbucket.org/Olololshka/libsettingscmdline.git

    * Создаем каталог для сборки
    $ mkdir libsettingscmdline/build && cd libsettingscmdline/build

    * запуск cmake
    ** для windows:
            $ cmake .. -G"MSYS Makefiles"
    ** для Linux:
            $ cmake ..
      В процессе могут возниукнуть ошибки, по причине нехватки некоторых обязательных компанентов.
      Воспользуйтесь документацией чтобы устранить эти ошибки.

    * Сборка
    make

    * Установка (для Linux требуются права администратора).
      Каталог установки по-умолчанию /usr/local, что соответствует
      <каталог установки MinGW>/msys/1.0/local в Windows
    # make install

    * Сборка документации
    $ make doc
	
	
Использование:
	Требуется создание промежуточного класса-наследника от cSettingsCmdLine.
	Например:
	
	//---------------------------------------------------
	Settings.h
	//---------------------------------------------------
	
	#ifndef SETTINGS_H_
	#define SETTINGS_H_

	#include <QMap>
	#include <QString>
	#include <QVariant>
	 
	#include <settingscmdline/SimpleOpt.h>
	#include <settingscmdline/settingscmdline.h>

	class Settings: public SettingsCmdLine::cSettingsCmdLine
	{
	public:
		Settings(int argc, char *argv[]);
		static struct SettingsCmdLine::key_val_res parse_Arg(int argCode,
			const char* optText, char* ArgVal, cSettingsCmdLine* origins);
		static void SetDefaultValues(cSettingsCmdLine* _this);
		static void console_ShowUsage(cSettingsCmdLine* _this);
	 private:
		 static SettingsCmdLine::pfTable table;
	 };

	#endif /* SETTINGS_H_ */
	
	//---------------------------------------------------
	Settings.cpp
	//---------------------------------------------------
	
	#include "Settings.h"

	// коды ключей уомандной строки
	enum enArgs
	{
		OPT_HELP,
		OPT_A,
	};

	// таблица соответствия кодов и ключей командной строки (подробнее в документации на SimpleOpt)
	static CSimpleOpt::SOption g_rgOptions[] =
	{
		{
			OPT_HELP, _T("-h"), SO_NONE
		}, 
		{
			OPT_HELP, _T("--help"), SO_NONE
		}, 
		{
			OPT_A, _T("-a"), SO_NONE
		}, 
		SO_END_OF_OPTIONS
		// END
	};

	// таблица вызовов (не требует резактирования)
	SettingsCmdLine::pfTable Settings::table =
	{
		g_rgOptions,
		Settings::SetDefaultValues,
		Settings::console_ShowUsage,
		Settings::parse_Arg
	};

	// тело этой функции отображает справку
	void Settings::console_ShowUsage(cSettingsCmdLine* _this)
	{
	// показать справку (-h)
	}

	// парсер аргументов командной строки
	struct SettingsCmdLine::key_val_res Settings::parse_Arg(int optCode,
			  const char* optText, char *ArgVal, cSettingsCmdLine* origins)
	{
		SettingsCmdLine::key_val_res result =
		{
				QString(), QVariant(), true
		};
		switch (optCode)
		{
		case OPT_A:
			// при обнаружении ключа '-a'
			result.key = "somekey";
			result.val = someval; // значение любого типа
		default:
			result.res = false;
			break;
		}
		return result;
	}

	// заполнение настроек по умолчанию, после запуска программы
	void Settings::SetDefaultValues(cSettingsCmdLine* _this)
	{
		_this->insert("Somekey", SOMEVAL);
	}

	Settings::Settings(int argc, char *argv[]) :
		QObject(NULL), cSettingsCmdLine(argc, argv, "Settings.ini", &table)
        {                                               //   ^- имя файла настроек, оставьте
                                                        // пустым, если не используется
	}
	
	Теперь можно создавать обьект и использовать его как обычный QVariantMap
	
	<b>Например:</b>
	int main(int argc, char* argv[])
	{
	...
	
	Settings settings(argc, argv);
	if (settings["someval"].toUint() == 10)
		{
		// что-то сделать
		}
	
	if (какое-то_условие)
		settings["key1"] = "abcd";
		
	for (int i = settings["from"]; i < settings["to"]; ++i)
		{
		}
	...
	}
	
	При удалении обьекта settings все пары ключ - значение будут записаны в файл, если таковой был поределен.
