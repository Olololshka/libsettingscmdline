#include <cstdio>

#include <QStringList>

#include "settingscmdline.h"

SettingsCmdLine::cSettingsCmdLine* SettingsCmdLine::settings;

using namespace SettingsCmdLine;

cSettingsCmdLine::cSettingsCmdLine(int argc, char* argv[],
                                   const QString& SettingsFilename,
                                   struct pfTable *Table)
{
    table = Table;
    table->defaultValuesFun(this);
    if (!SettingsFilename.isEmpty())
    {
        SettingsInifile = new QSettings(SettingsFilename, QSettings::IniFormat);
        SettingsInifile->setIniCodec("UTF-8");
        foreach(QString key, SettingsInifile->allKeys())
            // провнрить, может ключь уже есть
            (*this)[key] = SettingsInifile->value(key);
    }
    else
        SettingsInifile = NULL;
    if (argc > 1)
    {
        fIsOk = true;
        CSimpleOpt arguments(argc, argv, table->options_Table);
        while (arguments.Next())
        {
            if (arguments.LastError() == SO_SUCCESS)
            {
                if (arguments.OptionId() == 0)
                {
                    table->usageHelpDisplayFun(this);
                    fIsOk = false;
                    break;
                }
                else
                {
                    struct key_val_res result = table->parser(arguments.OptionId(),
                                                              arguments.OptionText(),
                                                              arguments.OptionArg(),
                                                              this);
                    if (!result.res)
                    {
                        fprintf(stderr, "Argument %s parse error.\n", arguments.OptionText());
                        fIsOk = false;
                        break;
                    }
                    else
                        (*this)[result.key] = result.val;
                }
            }
            else
            {
                fprintf(stderr, "Invalid argument: %s\n", arguments.OptionText());
                fIsOk = false;
                break;
            }
        }
    }
    else
        fIsOk = true;
    settings = this;
}

cSettingsCmdLine::~cSettingsCmdLine()
{
    if (SettingsInifile != NULL)
    {
        // обновляем значения ключей в файле
        foreach(QString key, this->keys())
            SettingsInifile->setValue(key, (*this)[key]);
        SettingsInifile->sync();
        delete SettingsInifile;
    }
}

QVariant cSettingsCmdLine::valueIfExists(QString key, QVariant defaultVal) const
{
    QVariantMap::const_iterator it = this->find(key);
    if (it != this->end())
        return *it;
    else
        return defaultVal;
}

bool cSettingsCmdLine::isOk() const
{
    return fIsOk;
}

